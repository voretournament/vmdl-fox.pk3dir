textures/player/fox_body
{
	dpreflectcube cubemaps/default/sky
	{
		map textures/player/fox_body.tga
		rgbgen lightingDiffuse
	}
}

textures/player/fox_armor
{
	dpreflectcube cubemaps/default/sky
	{
		map textures/player/fox_armor.tga
		rgbgen lightingDiffuse
	}
}

textures/player/fox_gullet
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 25 sin 0 0.25 0 2
	{
		map textures/player/fox_gullet.tga
		rgbgen lightingDiffuse
	}
}

textures/player/fox_stomach
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 100 sin 0 2.5 0 3
	{
		map textures/player/fox_stomach.tga
		rgbgen lightingDiffuse
	}
}
